# Templafy Api Wrapper

A simple API wrapper for Templafy https://www.templafy.com Allows for simple communication from PHP with Templafy with 
minimal setup.

There seems to be two versions of the API available, this api wrapper uses https://api-v1.templafy.com/swagger/ui/index#/.
This version of the API seems more intended for partners and allows direct interface towards a "channelId" without knowing
what library that channel belongs to.

## Requirements
 * PHP 7.2+
 * Guzzle 6.1+

## Installation
`composer require qbnk/templafy-apiwrapper ^0.1`

## Authorization
Templafy's API uses OAuth2 with the client credentials flow. This means that you will need a *client id* and a *client 
secret* to be able to authorize with and use the API. The *client id* is the subdomain of the templafy instance. If the 
instance domain is `customer.templafy.com`, the *client id* is `customer`. The *client secret* is available in the 
Templafy administration.

### Example
To create an instance that can authorize with the API, simply pass a `Credentials` instance with the *client id* and 
*client secret* to the `TemplafyApi` constructor. The library will then handle the rest.
```php
$api = new TemplafyApi(new Credentials('customer', 'client secret'));
``` 

## FAQ
##### How do I create a folder in the absolute root?
You have to pass a `parentFolderId` for all folders that shall be created. This value is the id of the image library 
itself when creating root folders. You can find this value in the URL in the Templafy administration when viewing the 
image library. It is the query parameter `categoryId`.