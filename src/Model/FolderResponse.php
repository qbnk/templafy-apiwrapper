<?php


namespace QBNK\TemplafyApi\Model;


class FolderResponse extends Folder {

	/** @var string */
	protected $id;

	/** @var string */
	protected $navigationPath;

	/**
	 * @param string $id
	 * @return self
	 */
	protected function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id ?? '';
	}

	/**
	 * @param string $navigationPath
	 * @return self
	 */
	protected function setNavigationPath(string $navigationPath) {
		$this->navigationPath = $navigationPath;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNavigationPath(): string {
		return $this->navigationPath ?? '';
	}

	public static function fromArray(array $data) {
		$instance = parent::fromArray($data);
		$instance
			->setId($data['id'])
			->setNavigationPath($data['navigationPath'])
		;
		return $instance;
	}
}