<?php


namespace QBNK\TemplafyApi\Model;


use JsonSerializable;

class Folder implements JsonSerializable {

	/** @var string */
	protected $name;

	/** @var string */
	protected $parentFolderId;

	/**
	 * @param string $name
	 * @return self
	 */
	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name ?? '';
	}

	/**
	 * @param string $parentFolderId
	 * @return self
	 */
	public function setParentFolderId(string $parentFolderId) {
		$this->parentFolderId = $parentFolderId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getParentFolderId(): string {
		return $this->parentFolderId ?? '';
	}

	public static function fromArray(array $data) {
		$instance = new static();
		$instance
			->setName($data['name'])
			->setParentFolderId($data['parentFolderId'])
		;
		return $instance;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return [
			'name' => $this->getName(),
			'parentFolderId' => $this->getParentFolderId()
		];
	}
}