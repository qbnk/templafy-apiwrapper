<?php
declare(strict_types=1);


namespace QBNK\TemplafyApi\Model;


class ImageResponse extends Image {

	/** @var string */
	protected $id;

	/** @var string */
	protected $mimeType;

	/** @var int */
	protected $width;

	/** @var int */
	protected $height;

	/** @var int */
	protected $fileSize;

	/** @var string */
	protected $checksum;

	/** @var string */
	protected $previewUrl;

	/** @var string */
	protected $downloadUrl;

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getMimeType(): string {
		return $this->mimeType;
	}

	/**
	 * @return int
	 */
	public function getWidth(): int {
		return $this->width;
	}

	/**
	 * @return int
	 */
	public function getHeight(): int {
		return $this->height;
	}

	/**
	 * @return int
	 */
	public function getFileSize(): int {
		return $this->fileSize;
	}

	/**
	 * @return string
	 */
	public function getChecksum(): string {
		return $this->checksum;
	}

	/**
	 * @return string
	 */
	public function getPreviewUrl(): string {
		return $this->previewUrl;
	}

	/**
	 * @return string
	 */
	public function getDownloadUrl(): string {
		return $this->downloadUrl;
	}

	/**
	 * @param string $id
	 * @return ImageResponse
	 */
	protected function setId(string $id): ImageResponse {
		$this->id = $id;
		return $this;
	}

	/**
	 * @param string $mimeType
	 * @return ImageResponse
	 */
	protected function setMimeType(string $mimeType): ImageResponse {
		$this->mimeType = $mimeType;
		return $this;
	}

	/**
	 * @param int $width
	 * @return ImageResponse
	 */
	protected function setWidth(int $width): ImageResponse {
		$this->width = $width;
		return $this;
	}

	/**
	 * @param int $height
	 * @return ImageResponse
	 */
	protected function setHeight(int $height): ImageResponse {
		$this->height = $height;
		return $this;
	}

	/**
	 * @param int $fileSize
	 * @return ImageResponse
	 */
	protected function setFileSize(int $fileSize): ImageResponse {
		$this->fileSize = $fileSize;
		return $this;
	}

	/**
	 * @param string $checksum
	 * @return ImageResponse
	 */
	protected function setChecksum(string $checksum): ImageResponse {
		$this->checksum = $checksum;
		return $this;
	}

	/**
	 * @param string $previewUrl
	 * @return ImageResponse
	 */
	protected function setPreviewUrl(string $previewUrl): ImageResponse {
		$this->previewUrl = $previewUrl;
		return $this;
	}

	/**
	 * @param string $downloadUrl
	 * @return ImageResponse
	 */
	protected function setDownloadUrl(string $downloadUrl): ImageResponse {
		$this->downloadUrl = $downloadUrl;
		return $this;
	}

	/**
	 * @return ImageResponse
	 */
	public static function fromArray(array $data) {
		$instance = parent::fromArray($data);
		$instance
			->setId($data['id'])
			->setMimeType((string)$data['mimeType'])
			->setWidth((int)$data['width'])
			->setHeight((int)$data['height'])
			->setFileSize((int)$data['fileSize'])
			->setChecksum((string)$data['checksum'])
			->setPreviewUrl((string)$data['previewUrl'])
			->setDownloadUrl((string)$data['downloadUrl'])
		;
		return $instance;
	}
}