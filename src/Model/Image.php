<?php
declare(strict_types=1);


namespace QBNK\TemplafyApi\Model;


use JsonSerializable;

class Image implements JsonSerializable {

	/** @var string */
	protected $name;

	/** @var string */
	protected $folderId;

	/** @var string */
	protected $tags;

	/** @var string */
	protected $externalData;

	/**
	 * @param string $name
	 * @return Image
	 */
	public function setName(string $name): Image {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $folderId
	 * @return Image
	 */
	public function setFolderId(string $folderId): Image {
		$this->folderId = $folderId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFolderId(): string {
		return $this->folderId;
	}

	/**
	 * @param string $tags
	 * @return Image
	 */
	public function setTags(string $tags): Image {
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTags(): string {
		return $this->tags ?? '';
	}

	/**
	 * @param string $externalData
	 * @return Image
	 */
	public function setExternalData(string $externalData): Image {
		$this->externalData = $externalData;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getExternalData(): string {
		return $this->externalData ?? '';
	}

	/**
	 * @return Image
	 */
	public static function fromArray(array $data) {
		$instance = new static();
		$instance
			->setName((string)$data['name'])
			->setTags((string)$data['tags'])
			->setFolderId((string)$data['folderId'])
			->setExternalData((string)$data['externalData'])
		;
		return $instance;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return [
			'name' => $this->getName(),
			'folderId' => $this->getFolderId(),
			'tags' => $this->getTags(),
			'externalData' => $this->getExternalData()
		];
	}
}