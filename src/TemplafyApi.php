<?php
declare(strict_types=1);

namespace QBNK\TemplafyApi;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use QBNK\Guzzle\Oauth2\GrantType\ClientCredentials;
use QBNK\Guzzle\Oauth2\GrantType\GrantTypeBase;
use QBNK\Guzzle\Oauth2\GrantType\RefreshToken;
use QBNK\Guzzle\Oauth2\Middleware\OAuthMiddleware;
use QBNK\TemplafyApi\Controller\FolderController;
use QBNK\TemplafyApi\Controller\ImageController;

class TemplafyApi {

	public const API_URL = 'https://api-v1.templafy.com';
	public const API_TOKEN_URL = 'oauth2/token';

	/** @var Credentials */
	protected $credentials;

	/** @var Client */
	protected $apiClient;

	/** @var OAuthMiddleware */
	protected $oauth2Middleware;

	/** @var FolderController */
	protected $folderController;

	/** @var ImageController */
	protected $imageController;

	public function __construct(Credentials $credentials, array $options = []) {
		$this->credentials = $credentials;
	}

	public function getHttpClient(): Client {
		if (!($this->apiClient instanceof Client)) {
			$handlerStack = HandlerStack::create();

			$oauthClient = new Client([
				'base_uri' => self::API_URL,
				'verify' => true,
				'headers' => [
					'User-Agent' => 'templafy-apiwrapper/1 (guzzle: 6)',
				],
			]);
			$config = [
				GrantTypeBase::CONFIG_CLIENT_ID => $this->credentials->getClientId(),
				GrantTypeBase::CONFIG_CLIENT_SECRET => $this->credentials->getClientSecret(),
				GrantTypeBase::CONFIG_TOKEN_URL => self::API_TOKEN_URL
			];
			$this->oauth2Middleware = new OAuthMiddleware(
				$oauthClient,
				new ClientCredentials($oauthClient, $config),
				new RefreshToken($oauthClient, $config)
			);
			$handlerStack->push($this->oauth2Middleware->onBefore());
			$handlerStack->push($this->oauth2Middleware->onFailure(3));


			$apiClient = new Client([
				'handler' => $handlerStack,
				'auth' => 'oauth2',
				'base_uri' => self::API_URL,
				'headers' => [
					'Accept' => 'application/json',
					'Content-type' => 'application/json',
					'User-Agent' => 'templafy-apiwrapper/1 (guzzle: 6)',
				]
			]);
			$this->apiClient = $apiClient;
		}

		return $this->apiClient;
	}

	public function folders(): FolderController {
		if (!($this->folderController instanceof FolderController)) {
			$this->folderController = new FolderController($this->getHttpClient());
		}
		return $this->folderController;
	}

	public function images(): ImageController {
		if (!($this->imageController instanceof ImageController)) {
			$this->imageController = new ImageController($this->getHttpClient());
		}
		return $this->imageController;
	}
}