<?php
declare(strict_types=1);

namespace QBNK\TemplafyApi;

class Credentials {

	/** @var string */
	protected $clientId;

	/** @var string */
	protected $username;

	/**
	* @param string $clientId
	* @param string $clientSecret
	*/
	public function __construct($clientId, $clientSecret) {
		$this->clientId = $clientId;
		$this->clientSecret($clientSecret);
	}

	/**
	 * Gets or sets the internal value
	 * @internal Hack to hide value from dumping and possibly exposing by mistake.
	 * @param string|null $newClientSecret
	 * @return string
	 */
	protected function clientSecret($newClientSecret = null): string {
		static $clientSecret;
			if ($newClientSecret !== null) {
				$clientSecret = $newClientSecret;
			}
		return $clientSecret ?? '';
	}

	public function getClientId(): string {
		return $this->clientId;
	}

	public function getClientSecret(): string {
		return $this->clientSecret();
	}
}