<?php
declare(strict_types=1);

namespace QBNK\TemplafyApi\Controller;

use GuzzleHttp\Client;

class BaseController {

    /**
     * Guessed the values in this list from mentions in description of parameter in API documentation
     * @var string[]
     */
    public const LIBRARY_TYPES = [
        'Image',
        'Document',
        'PDF',
        'Presentation',
        'Slide'
    ];

	/** @var Client */
	protected $apiHttpClient;

	public function __construct($apiHttpClient) {
		$this->apiHttpClient = $apiHttpClient;
	}
}