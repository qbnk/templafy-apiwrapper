<?php

declare(strict_types=1);

namespace QBNK\TemplafyApi\Controller;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Utils;
use QBNK\TemplafyApi\Model\Image;
use QBNK\TemplafyApi\Model\ImageResponse;

class ImageController extends BaseController
{

    /**
     * @param string|null $folderId
     * @param string|null $navigationPath
     * @param string|null $searchQuery
     * @param int|null $pageNumber
     * @param int|null $pageSize
     * @return ImageResponse[]
     */
    public function list(
        string $folderId = null,
        string $navigationPath = null,
        string $searchQuery = null,
        int $pageNumber = null,
        int $pageSize = null
    ): array
    {
        $queryParameters = [];
        if ($folderId !== null) {
            $queryParameters['folderId'] = $folderId;
        }
        if ($navigationPath !== null) {
            $queryParameters['navigationPath'] = $navigationPath;
        }
        if ($searchQuery !== null) {
            $queryParameters['searchQuery'] = $searchQuery;
        }
        if ($pageNumber !== null) {
            $queryParameters['pageNumber'] = $pageNumber;
        }
        if ($pageSize !== null) {
            $queryParameters['pageSize'] = $pageSize;
        }
        $response = $this->apiHttpClient->get('/images', ['query' => $queryParameters]);
        $responseData = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        $result = [];
        foreach ($responseData as $rawFolder) {
            $result[] = ImageResponse::fromArray($rawFolder);
        }

        return $result;
    }

    public function create(Image $image, string $pathToFile): ImageResponse
    {
        $response = $this->apiHttpClient->post('/images', [
            'multipart' => [
                ['name' => 'name', 'contents' => $image->getName()],
                ['name' => 'folderId', 'contents' => $image->getFolderId()],
                ['name' => 'tags', 'contents' => $image->getTags()],
                ['name' => 'externalData', 'contents' => $image->getExternalData()],
                ['name' => 'file', 'contents' => fopen($pathToFile, 'r')]
            ],
            'headers' => null
        ]);
        if ($response->getStatusCode() !== 201) {
            throw new TransferException(
                'Non-successful response to upload: ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase()
            );
        }
        $assetUrl = $response->getHeader('Location')[0];
        $imageId = str_replace('https://api-v1.templafy.com/images/', '', $assetUrl);

        // Sometimes the API doesn't realize the image we just uploaded exists. Retry up to 5 times.
        return $this->retryGuzzle(5, function () use ($imageId) {
            return $this->get($imageId);
        });
    }

    public function get(string $id): ImageResponse
    {
        $response = $this->apiHttpClient->get('/images/' . $id);
        $responseData = Utils::jsonDecode($response->getBody()->getContents(), true);
        return ImageResponse::fromArray($responseData);
    }

    public function update(string $id, Image $image, string $pathToFile): ImageResponse
    {
        $response = $this->apiHttpClient->patch('/images/' . $id, [
            'multipart' => [
                ['name' => 'name', 'contents' => $image->getName()],
                ['name' => 'folderId', 'contents' => $image->getFolderId()],
                ['name' => 'tags', 'contents' => $image->getTags()],
                ['name' => 'externalData', 'contents' => $image->getExternalData()],
                ['name' => 'file', 'contents' => fopen($pathToFile, 'r')]
            ],
            'headers' => null
        ]);
        return $this->get($id);
    }

    public function delete(string $id): void
    {
        $this->apiHttpClient->delete('/images/' . $id);
    }

    /**
     * Retries $fn up to $retries times with exponential back off when 4xx responses are returned.
     * @param int $retries
     * @param callable $fn
     * @return mixed The return value from $fn
     * @throws ClientException Thrown when $retries attempts has been made. Is the last real ClientException.
     */
    protected function retryGuzzle(int $retries, callable $fn)
    {
        $attempts = 1;
        beginning:
        try {
            return $fn();
        } catch (ClientException $ce) {
            if (!$retries) {
                throw $ce;
            }
            $retries--;
            sleep(2 ** $attempts);   // Exponential back off
            $attempts++;
            goto beginning;
        }
    }
}