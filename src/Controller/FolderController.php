<?php

declare(strict_types=1);

namespace QBNK\TemplafyApi\Controller;

use QBNK\TemplafyApi\Model\Folder;
use QBNK\TemplafyApi\Model\FolderResponse;

class FolderController extends BaseController
{

    /**
     * @param string|null $parentFolderId
     * @param string|null $navigationPath Hierarchical path identifying a parent folder
     * @param string $libraryType
     * @return FolderResponse[]
     */
    public function list(
        string $parentFolderId = null,
        string $navigationPath = null,
        string $libraryType = 'Image'
    ): array {
        $queryParameters = ['libraryId' => $libraryType];
        if ($parentFolderId !== null) {
            $queryParameters['parentFolderId'] = $parentFolderId;
        }
        if ($navigationPath !== null) {
            $queryParameters['navigationPath'] = $navigationPath;
        }
        $response = $this->apiHttpClient->get('/folders', ['query' => $queryParameters]);
        $responseData = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        $result = [];
        foreach ($responseData as $rawFolder) {
            $result[] = FolderResponse::fromArray($rawFolder);
        }

        return $result;
    }

    public function create(Folder $folder): FolderResponse
    {
        $response = $this->apiHttpClient->post('/folders', ['json' => $folder]);
        $folderId = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        return FolderResponse::fromArray([
            'id' => $folderId,
            'name' => $folder->getName(),
            'parentFolderId' => $folder->getParentFolderId(),
            'navigationPath' => ''
        ]);
    }

    public function get(string $id): FolderResponse
    {
        $response = $this->apiHttpClient->get('/folders/' . $id);
        $responseData = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        return FolderResponse::fromArray($responseData);
    }

    public function update(string $id, Folder $folder): void
    {
        $this->apiHttpClient->put('folders/' . $id, ['json' => $folder]);
    }

    public function delete(string $id): void
    {
        $this->apiHttpClient->delete('/folders/' . $id);
    }
}